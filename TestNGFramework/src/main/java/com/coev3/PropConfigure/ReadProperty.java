package com.coev3.PropConfigure;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ReadProperty
{
	 public static Logger log = Logger.getLogger("devpinoyLogger");

	 
	    public static Properties prop;

	    public static Properties init(String path) {

	        if (prop == null) {

	            //Configure the location of property file.
	           // String path = System.getProperty("user.dir") + "/src/test/resources/project.properties";
	         

	          prop = new Properties();

	        try {
	                FileInputStream fs = new FileInputStream(path);

	              prop.load(fs);
	                log.info("file loaded properly");

	            } catch (Exception e) {

	                log.error("There is no Property file on this location == " + path);

	                //Printing server error information.
	                e.printStackTrace();
	            }
	        }
	        return prop;
	    }

}
