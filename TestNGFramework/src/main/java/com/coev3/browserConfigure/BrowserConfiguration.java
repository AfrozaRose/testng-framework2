package com.coev3.browserConfigure;

import java.net.URL;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.safari.SafariDriver;



public class BrowserConfiguration {
	
	public static Logger log = Logger.getLogger("devpinoyLogger");
		public  WebDriver driver;
		
	    public  WebDriver testLocal(String browserName) {

	        try {
             if (browserName.equalsIgnoreCase("chrome")) {

	                   log.info("Open Browser -- Chrome");
	                   
	System.setProperty("webdriver.chrome.driver","C:\\Users\\p005020f\\Downloads\\chromedriver_win32\\chromedriver.exe" );

	                    driver = new ChromeDriver();
	
             } else if (browserName.equalsIgnoreCase("safari")) {

	              log.info("Open Browser -- Safari");

	              driver = new SafariDriver();

	                  } else if (browserName.equalsIgnoreCase("firefox")) {

	                    log.info("Open Browser -- Firefox");
	                    
System.setProperty("webdriver.gecko.driver", "C:\\Users\\p005020f\\Downloads\\geckodriver-v0.26.0-win64 (1)\\geckodriver.exe");
                  driver = new FirefoxDriver();
	                }
	            driver.manage().window().maximize();

	            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	            } catch (Exception e) {
	e.printStackTrace();
	e.getMessage();

	       
	         }
	       return driver;

	}

		public void navigate(String property) {
			
			
		}
		}

	

	

