package com.coev3.test;

import org.testng.annotations.Test;

import com.coev3.PropConfigure.ReadProperty;
import com.coev3.browserConfigure.BrowserConfiguration;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class BaseTest{
	
	 public static Logger log = Logger.getLogger("devpinoyLogger");
	public  WebDriver driver;
	Properties prop;
	BrowserConfiguration browse;
	public static ExtentReports reports;
	public static ExtentTest test;
	
	 @BeforeSuite
	  public void configPropertyFile() {
		log.info("project is starting");
		//Initializing extent reports
		String extentFilePath = System.getProperty("user.dir") + "/ExtentsReportsFolder/extentReport.html";
		reports = new ExtentReports(extentFilePath);
		
		 }
	 
	  @BeforeTest
	  public void beforeTest() {
		  String path = System.getProperty("user.dir") + "/runConfigure.properties";
			 prop=ReadProperty.init(path);
			 log.info(prop.getProperty("browser"));
			 
			
			   }
	  
	  @BeforeClass

 
	  @BeforeMethod
	  public void beforeMethod(ITestResult result){
		  browse =new BrowserConfiguration();
		 String run= prop.getProperty("runMode");
		 if(run.equalsIgnoreCase("local"))
		 {
		  driver= browse.testLocal(prop.getProperty("browser"));
		  
		 }

		  
		    browse.navigate(prop.getProperty("url"));
		
		  test = reports.startTest(result.getMethod().getMethodName());
		  test.log(LogStatus.INFO, result.getMethod().getMethodName() + " has started");
 
		  
	  }

	  @AfterMethod
	 
	    public void closeBrowser(ITestResult result) {
		  
		  
		      if(result.getStatus()== ITestResult.SUCCESS) {
		    	  test.log(LogStatus.PASS, result.getMethod().getMethodName() + " is successful");
		      }
		  
			  else if(result.getStatus()== ITestResult.FAILURE) {

			  File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			  try {

				  File destination = new File(System.getProperty("user.dir") + "/ScreenShots/" + result.getMethod().getMethodName() + ".png");
				  FileUtils.copyFile(src, destination);
				  String path = destination.getAbsolutePath();
				  String file = test.addScreenCapture(path);
				  test.log(LogStatus.FAIL, result.getMethod().getMethodName() + " is failed", file);
				  
			  
			  }
			  
			  catch (IOException e)
			   {
			    System.out.println(e.getMessage());
			   
			   }
			  
		  }
			  else 
				  test.log(LogStatus.SKIP, result.getMethod().getMethodName() + " is skipped");
	       
		  
		  
		
	        if(driver != null) {

	            
	            driver.quit();

	        
	            log.info("Close Browser");

	        }

	    }

 


	  @AfterSuite
	  public void afterSuite() {
		  reports.endTest(test);
		  reports.flush();
	  
	  }

}

